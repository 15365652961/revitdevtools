﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB.Plumbing;

namespace RevitDevBase
{
     public class ElementList
    {
        public void Excute()
        {
            TaskDialog.Show("00", "00");
        }
        /// <summary>
        /// 获取所有模型元素
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> GetAllModelElements(Document doc)
        {
            List<Element> elements = new List<Element>();

            FilteredElementCollector collector= new FilteredElementCollector(doc) .WhereElementIsNotElementType();

            foreach (Element e in collector)
            {
                if (null != e.Category && e.Category.HasMaterialQuantities)
                {
                    elements.Add(e);
                }
            }
            return elements;
        }
        
        public static List<List<Element>> RetrieveAllFamily(Document doc)
        {
            List<List<Element>> elements = new List<List<Element>>();

            elements.Add(RetrieveAllRoom(doc));
            elements.Add(RetrieveAllModelGroups(doc));
            elements.Add(RetrieveAllDetailGroups(doc));
            elements.Add(RetrieveAllWall(doc));
            elements.Add(RetrieveAllFloor(doc));
            elements.Add(RetrieveAllRoof(doc));
            elements.Add(RetrieveAllPipe(doc));
            elements.Add(RetrieveAllModelText(doc));
            elements.Add(RetrieveAllInstance(doc));
            elements.Add(RetrieveAllRoomSeparate(doc));
            elements.Add(RetrieveAllModelLine(doc));

            elements.Add(RetrieveAllTextNote(doc));
            
            return elements;
        }

        /// <summary>
        /// 获取所有族实例
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllInstance(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfClass(typeof(FamilyInstance)).ToList();
            return elements;
        }

        //逻辑对象

        /// <summary>
        /// 获取所有模型组
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllModelGroups(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_IOSModelGroups).OfClass(typeof(Group)).ToList();
            return elements;
        }

        /// <summary>
        /// 获取所有详图组
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllDetailGroups(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_IOSDetailGroups).OfClass(typeof(Group)).ToList();
            return elements;
        }
        
        /// <summary>
        /// 获取所有房间
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllRoom(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_Rooms).OfClass(typeof(SpatialElement)).ToList();       
            return elements;
        }

        /// <summary>
        /// 获取所有房间分隔
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllRoomSeparate(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_RoomSeparationLines).ToList();
            return elements;
        }

        /// <summary>
        /// 获取所有线
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllModelLine(Document doc)
        {
            
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_Lines).OfClass(typeof(CurveElement)).ToList();
            foreach (var line in elements.ToArray())
            {
                if (line.Name!="模型线")
                {
                    elements.Remove(line);
                }
            }
            return elements;
        }
       
        //三维系统族

        /// <summary>
        /// 获取所有墙模型
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllWall(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_Walls).OfClass(typeof(Wall)).ToList();
            return elements;
        }

        /// <summary>
        /// 获取所有楼板
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllFloor(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_Floors).OfClass(typeof(Floor)).ToList();
            return elements;
        }

        /// <summary>
        /// 获取所有屋顶
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllRoof(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_Roofs).ToList();
            return elements;
        }

        /// <summary>
        /// 获取所有管道
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllPipe(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_PipeCurves).OfClass(typeof(Pipe)).ToList();
            return elements;
        }

        /// <summary>
        /// 获取所有模型文字
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllModelText(Document doc)
        {
            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_GenericModel).OfClass(typeof(ModelText)).ToList();
            return elements;
        }

        //平面系统族

        /// <summary>
        /// 获取所有文本
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Element> RetrieveAllTextNote(Document doc)
        {

            List<Element> elements = new FilteredElementCollector(doc).WhereElementIsNotElementType()
                .OfCategory(BuiltInCategory.OST_TextNotes).ToList();
            
            return elements;
        }
        
    }
}
